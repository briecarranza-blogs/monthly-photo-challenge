# Monthly Photo Challenge

  1. Take a look at the challenge image for a month
  2. Take an image to respond to the challenge
  3. Add your entry to the appropriate folder in [photos](photos/) for the month you are responding to

Note: use this format for the name of your image: `entry-username.md` where `username` is your work GitLab username. If you want to add any notes, you can do so in a file called `entry-username.txt` in the same folder. 


The website will be updated to include your entry. The website is built using `Expose`, a [simple static site generator for photoessays](https://github.com/Jack000/Expose). 


## TODOs

  - [ ] Add note about license of uploaded images
  - [ ] Set up Pages
  - [ ] Add URL to [README.md](README.md)